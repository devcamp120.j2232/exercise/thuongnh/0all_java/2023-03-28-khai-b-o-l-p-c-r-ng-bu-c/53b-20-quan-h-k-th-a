public class App {
    public static void main(String[] args) throws Exception {

        // khởi tạo hai đối tượng hình tròn
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, "green");
        // in thông tin 3 đối tượng trên màn hình 
        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle3);
        // in thông tin diện tích 3 đối tượng 
        System.out.println(circle1.getArea());
        System.out.println(circle2.getArea());
        System.out.println(circle3.getArea());



        // yc 5  tạo ra 4 đối tượng hình trụ
        Cylinder cylinder1 =  new Cylinder();
        Cylinder cylinder2 =  new Cylinder(2.5);
        Cylinder cylinder3 =  new Cylinder(3.5,1.5);
        Cylinder cylinder4 =  new Cylinder(3.5, "green", 1.5);

        // in 4 đối tượng trên ra màn hình
        System.out.println(cylinder1);
        System.out.println(cylinder2);
        System.out.println(cylinder3);
        System.out.println(cylinder4);
        

        System.out.println(cylinder1.getArea());
        System.out.println(cylinder2.getArea());
        System.out.println(cylinder3.getArea());
        System.out.println(cylinder4.getArea());



    }
}
