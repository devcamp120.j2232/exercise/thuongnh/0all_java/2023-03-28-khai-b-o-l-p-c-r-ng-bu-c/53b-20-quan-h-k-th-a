public class Cylinder extends Circle   {
    private double hight = 1.0;

   
    public Cylinder() {
        super();
    }
    public Cylinder(double radius) {
        this.hight = hight;
    }

    public Cylinder(double radius, double hight) {
        super(radius);
        this.hight = hight;
    }

    public Cylinder(double radius, String color, double hight) {
        super(radius, color);
        this.hight = hight;
    }

  

    public double getHeight() {
        return hight;
    }

    public void setHight(double hight) {
        this.hight = hight;
    }


    public double getVolume(){
        return getArea() * hight;
    }
    @Override
    public String toString() {
        return "Cylinder [ radius= " + this.getRadius() +   "  corlor= " + this.getColor() + "  hight=" + hight + "]";
    }

    

    

    
}
